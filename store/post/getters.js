export default {
    getAllPosts(state) {
        return state.posts
    },
    getPostById: (state) => (id) => {
        return state.posts.find(posts => posts.id === id)
    }
}