export default {
    getParticipants(state) {
        return state.participants
    },
    getMyself(state) {
        return state.myself
    },
    getMessages(state) {
        return state.messages
    },
    getChatTitle(state) {
        return state.chatTitle
    },
    getToLoad(state) {
        return state.toLoad
    }
}